import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import LoginNav from '../Navbar/login-nav';
import { FormGroup, InputGroup, Button, Card, Elevation } from "@blueprintjs/core";
import './join.css';

export const Join = () => {

    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    const [warn, setWarn] = useState(false);

    const warningCard = () => {
        return(
            <Card className="warnCard" interactive={true} elevation={Elevation.TWO}>
                <h3><a href="#">Warning</a></h3>
                <p><b>Fill both Name and Room field to proceed...</b></p>
                <Button onClick={()=>{setWarn(false)}}>close</Button>
            </Card>
        )
    }

    const enterChat = (event) => {
        event.preventDefault()
        if(name && room){
            window.location.href = `http://${window.location.host}/chat?name=${name}&room=${room}`;
        }else{
            setWarn(true)
        }
    }

    return (
        <>
            <LoginNav />
            {warn ? warningCard() : null}
            <div className="joinOuterContainer">
                <div className="joinInnerContainer">
                    <form onSubmit={event => enterChat(event)}>
                        <FormGroup
                            helperText="Fill name and room id to enter chat room!"
                            label="JOIN"
                            className="FormBlueprintJSReqs">
                            <InputGroup
                                placeholder="Name"
                                round="true"
                                large="true"
                                type="text"
                                onChange={ (event) => setName(event.target.value) }
                            />
                            <InputGroup
                                placeholder="Room"
                                round="true"
                                large="true"
                                type="text"
                                onChange={ (event) => setRoom(event.target.value) }
                            />
                            <Button
                                text="Enter"
                                type="submit"
                                minimal
                            />
                        </FormGroup>
                    </form>
                </div>
            </div>
        </>
    )
}

// export default Join;