import React from 'react';
import { InputGroup, Button } from "@blueprintjs/core";
import './input.css';
import sendIcon from '../../icons/sendIcon.png';

const Input = ({ message, setMessage, sendMessage}) => (
    <form className="form">
        <InputGroup
            placeholder="type message here..."
            large="true"
            type="text"
            className="form-input"
            onChange={(event) => setMessage(event.target.value)}
            value={message}
            onKeyPress={event => event.key === 'Enter' ? sendMessage(event) : null }
            rightElement={
                <img
                    src={sendIcon}
                    style={{"width": "35px", "height": "35px"}}
                    onClick={(event) => sendMessage(event)}
                />
            }
        />
    </form>
)

export default Input;