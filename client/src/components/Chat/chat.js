import React, { useState, useEffect} from 'react';
import queryString from 'query-string';
import io from 'socket.io-client';
import InfoBar from '../InfoBar/infobar'
import Input from '../Input/input'
import Messages from '../Messages/messages'
import LoginNav from '../Navbar/login-nav';

import { Colors } from "@blueprintjs/core";
import './chat.css';


let socket;

export const Chat = ({location}) => {
    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    const [users, setUsers] = useState('');
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);
    
    const ENDPOINT = 'localhost:5000';

    useEffect(() => {
        const { name, room } = queryString.parse(location.search);

        socket = io(ENDPOINT);

        setName(name);
        setRoom(room);

        // console.log(socket)
        socket.emit('join', {name, room}, (error) => {
            if(error){
                alert(error)
            }
        });

        // acts as clean up similar to componentDidUnmount
        return () => {
            // When component unmouts socket instance is turned off
            socket.emit('disconnect');
            socket.off();
        }

    }, [ENDPOINT, location.search])
    
    // React hooks
    useEffect(() => {

        socket.on('message', (message) => {
            setMessages(messages => [...messages, message])
        })

        socket.on("roomData", ({ users }) => {
            setUsers(users);
        });

    }, [])

    const sendMessage = (event) => {
        event.preventDefault();
        if(message) {
            socket.emit('sendMessage', message, () => setMessage(''));
        }
        // socket.on('noUser', () => {
        //     window.location.href = '/';
        // })
    }

    // console.log(message, messages); 

    return (
        <>
            <LoginNav />
            <div className="outerContainer">
                <div className="container" style={{ color: Colors.BLUE3, background: Colors.BLACK }}>
                    <InfoBar room={room}/>
                    <Messages messages={messages} name={name} />
                    <Input message={message} setMessage={setMessage} sendMessage={sendMessage} />
                </div>
            </div>
        </>
    )
}

// export default Chat;