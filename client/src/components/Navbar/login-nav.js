import React from 'react';
import styles from './login-nav.css';
import { Navbar, Alignment, Button } from "@blueprintjs/core";
const LoginNav = () => (
    <Navbar className="bp3-dark">
        <Navbar.Group align={Alignment.LEFT}>
            <Navbar.Heading>The App For <b>CHAT</b></Navbar.Heading>
            <Navbar.Divider />
            {/* <Button className="bp3-minimal" icon="home" text="Home" /> */}
        </Navbar.Group>
    </Navbar>
)

export default LoginNav;