const express = require('express');
const router = express.Router();

/**
* @swagger
* /:
*   get:
*     tags:
*       - Server Root Endpoints
*     summary: To check if server is up.
*     description: This is root endpoint to check if server is up or not.
*     responses:
*       200:
*         description: Receive back response from server.
*/
router.get('/', (req, res) => {
    res.json({
        'Ok':'server is up and running',
        'status': 200
    });
})

module.exports = router;