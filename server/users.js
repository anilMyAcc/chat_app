// Stores users in list
const users = [];

// Funtion to add user to users list
const addUser = ({id, name, room}) => {
    name = name.trim().toLowerCase();
    room = room.trim().toLowerCase();

    // To check if user exists in users list
    const existingUser = users.find((user) => user.room === room && user.name === name);
    // If username is already taken  
    if(existingUser) {
        return { error: 'Username is taken' };
    }

    // if username do not exist add user to list
    const user = { id, name, room };
    users.push(user);

    return { user }
}

// Funtioon to remove user from users list
const removeUser = (id) => {

    const index = users.findIndex((user) => user.id === id);

    if(index !== -1 ){
        return users.splice(index, 1)[0]
    }
}

// Find user from users list using id
const getUser = (id) => users.find((user) => user.id === id);

// Get users in the same room
const getUserInRoom = (room) => users.filter((user) => user.room === room);

module.exports = { addUser, removeUser, getUser, getUserInRoom };