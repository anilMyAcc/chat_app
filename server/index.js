const express = require('express');
const swaggerUi = require('swagger-ui-express');
// const swaggerDocument = require('./swagger.json');
const socketio = require('socket.io');
const http = require('http');
const cors = require('cors');
const apiMetrics = require('prometheus-api-metrics');

const { addUser, removeUser, getUser, getUserInRoom } = require('./users')

const PORT = process.env.PORT || 5000;

const router = require('./router');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

io.on('connection', (socket) => {
    console.log('we have an new connection!!!');

    socket.on('join', ({name, room}, callback) => {

        const { error, user } = addUser({ id:socket.id, name, room})

        // console.log(name, room)
        // console.log("U have joined!!!")
        // const error = false;
        if(error) return callback(error)
        
        socket.emit('message', { user: 'admin', text: `${user.name}, welcome to the room - ${user.room}` });
        socket.broadcast.to(user.room).emit('message', { user: 'admin', text: `${user.name}, has joined!`});
        
        socket.join(user.room);

        io.to(user.room).emit('roomData', {room: user.room, users: getUserInRoom(user.room)});

        callback();
    })

    socket.on('sendMessage', (message, callback) => {
        const user = getUser(socket.id);
        io.to(user.room).emit('message', {user: user.name, text: message});
        io.to(user.room).emit('roomData', {user: user.room, users: getUserInRoom(user.room)});
        callback()
    })
    socket.on('disconnect', () => {
        console.log('User have left!!!')
        const user = removeUser(socket.id);

        if(user){
            io.to(user.room).emit('message', {user: 'admin', text: `${user.name} has left.`})
        }
    })
})

const swaggerJSDoc = require('swagger-jsdoc');

app.use(router);
app.use(cors());
const options = {
    definition: {
      openapi: '3.0.0', // Specification (optional, defaults to swagger: '2.0')
      info: {
        title: "Chat Bot - REST API's", // Title (required)
        version: '1.0.0', // Version (required)
      },
    },
    // Path to the API docs
    apis: ['./router.js'],
  };
const swaggerSpec = swaggerJSDoc(options);

app.get('/api-docs.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });
// app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use(apiMetrics());
server.listen(PORT, () => {
    console.log(`Server has started on port ${PORT}`)
})